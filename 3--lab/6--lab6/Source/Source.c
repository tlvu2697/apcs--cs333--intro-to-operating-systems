/* Includes */
#include <unistd.h>     /* Symbolic Constants */
#include <sys/types.h>  /* Primitive System Data Types */ 
#include <errno.h>      /* Errors */
#include <stdio.h>      /* Input/Output */
#include <stdlib.h>     /* General Utilities */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>     /* String handling */
#include <semaphore.h>  /* Semaphore */

/* prototype for thread routine */
void handler_hydrogen(void *ptr);
void handler_oxygen(void *ptr);
void handler_water(void *ptr);

/* global vars */
/* semaphores are declared global so they can be accessed 
   in main() and in thread routine,
   here, the semaphore is used as a mutex */
sem_t sem_hydrogen;
sem_t sem_oxygen;
sem_t sem_water;

int main() {
    int i[3];
    pthread_t thread_hydrogen;
    pthread_t thread_oxygen;
    pthread_t thread_water;
    
    i[0] = 0;	/* argument to threads */
    i[1] = 1;
    i[2] = 2;
    
    sem_init(&sem_hydrogen, 0, 2);      /* initialize mutex to 1 - binary semaphore */
                                 /* second param = 0 - semaphore is local */
    sem_init(&sem_oxygen, 0, 1);
    sem_init(&sem_water, 0, 0);
                                 
    /* Note: you can check if thread has been successfully created by checking return value of
       pthread_create */
    pthread_create (&thread_hydrogen, NULL, (void *) &handler_hydrogen, (void *) &i[0]);
    pthread_create (&thread_oxygen, NULL, (void *) &handler_oxygen, (void *) &i[1]);
    pthread_create (&thread_water, NULL, (void *) &handler_water, (void *) &i[2]);
        
    pthread_join(thread_hydrogen, NULL);
    pthread_join(thread_oxygen, NULL);
    pthread_join(thread_water, NULL);

    sem_destroy(&sem_hydrogen);	/* destroy semaphore */
    sem_destroy(&sem_oxygen);
    sem_destroy(&sem_water);
                  
    /* exit */  
    exit(0);
}	/* main() */

void handler_hydrogen(void *ptr) {
    while (1) {
    	sem_wait(&sem_hydrogen);
    	printf("H\n");
    	sem_post(&sem_water);
    }
    pthread_exit(0);	/* exit thread */
}

void handler_oxygen(void *ptr) {
    while (1) {
    	sem_wait(&sem_oxygen);
    	printf("O\n");
    	sem_post(&sem_water);
    }
    pthread_exit(0);	/* exit thread */
}

void handler_water(void *ptr) {
    while (1) {
    	sem_wait(&sem_water);
    	sem_wait(&sem_water);
    	sem_wait(&sem_water);
    	printf("--> H2O\n");
    	printf("---------\n");
    	sem_post(&sem_hydrogen);
    	sem_post(&sem_hydrogen);
    	sem_post(&sem_oxygen);
    }
    pthread_exit(0);	/* exit thread */
}
