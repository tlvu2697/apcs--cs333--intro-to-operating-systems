/* ascii.c 
 *    Test program to list ascii table.
 *
 */
#include "syscall.h"
int main() {
	int i;
	PrintString("---ASCII Table---\n");
	PrintString("Dec - Char\n");
    for (i = 0; i < 128; i++) {
    	PrintInt(i);
    	PrintString(" - ");
    	PrintChar((char) i);
    	PrintString("\n");
    }
}
