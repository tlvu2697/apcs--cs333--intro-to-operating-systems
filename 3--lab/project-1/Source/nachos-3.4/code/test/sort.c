/* sort.c 
 *    Test program to sort a large number of integers.
 *
 *    Intention is to stress virtual memory system.
 *
 *    Ideally, we could read the unsorted array off of the file system,
 *	and store the result back to the file system!
 */

#include "syscall.h"

int main() {
    int i, j, tmp, a[100], n;

    PrintString("-----Sort Program-----\n");
    PrintString("-Input:\n");
    PrintString("N = ");
    n = ReadInt();
    for (i = 0; i < n; i++) {
        PrintChar('#');
        PrintInt(i+1);
        PrintString(": ");
        a[i] = ReadInt();
    }
    PrintString("--&--\n\n");

    PrintString("-Sorting: ");
    for (i = 0; i < n-1; i++) {
        for (j = 0; j < n-i-1; j++) {
            PrintChar('#');
            if (Compare(a[j], a[j+1]) == 1) {
                tmp = a[j];
                a[j] = a[j+1];
                a[j+1] = tmp;
            }
        }
    }
    PrintString("\n--&--\n\n");

    PrintString("-Output:\n");
    for (i = 0; i < n; i++) {
        PrintInt(a[i]);
        PrintChar(' ');
    }
    PrintString("\n--&--\n");
}
