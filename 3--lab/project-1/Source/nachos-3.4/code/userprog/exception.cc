// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"

#define RET 2
#define ARG1 4
#define ARG2 5
#define ARG3 6
#define ARG4 7
#define INT_LENGTH 10
#define MAX_INT 65535

/*----------------------------------------------------------------------

// ExceptionHandler

Entry point into the Nachos kernel.  Called when a user program

/ executing, and either does a syscall, or generates an addressing
or arithmetic exception.
For system calls, the following is the calling convention:

system call code -- r2
arg1 -- r4
arg2 -- r5
arg3 -- r6
arg4 -- r7

The result of the system call, if any, must be put back into r2. 
And don't forget to increment the pc before returning. (Or else you'll
loop making the same system call forever
"which" is the kind of exception.  The list of possible exceptions 
are in machine.h.
//----------------------------------------------------------------------*/

void increasePC();
void Compare_SysCall();
void ReadChar_SysCall();
void PrintChar_SysCall();
void ReadInt_SysCall();
void PrintInt_SysCall();
void ReadString_SysCall();
void PrintString_SysCall();

char* User2System(int virtAddr, int limit);
int System2User(int virtAddr, int len, char* buffer);

void Compare_SysCall() {
    int op1, op2, result;
    op1 = machine->ReadRegister(4);
    op2 = machine->ReadRegister(5);
	result = op1 - op2;
    if (result > 0) {
        result = 1;
    } else if (result < 0) {
        result = -1;
    }
    machine->WriteRegister(2, result);
}

void ReadInt_SysCall(){
	int num, sign = 1;
	char c;
	do {
		gSynchConsole->Read(&c, 1);
	} while (c == '\n' || c == 0);
	if (c == '+' || c == '-') {
		if (c == '-')
			sign = -1;
		gSynchConsole->Read(&c, 1);
	}
	num = 0;
	while (c >= '0' && c <= '9') {
		num = num * 10 + (c - '0');
		gSynchConsole->Read(&c, 1);
	}
	num = sign * num;
	machine -> WriteRegister(2, num);
}

void PrintInt_SysCall() {
	char s[INT_LENGTH + 1], nega, c1;
	int i, n, length;
	i = 0;
	nega = '-';
	n = machine->ReadRegister(ARG1);
	if (n < 0)
	{
		gSynchConsole->Write(&nega,1);
		n = -n;
	}
	do {
		s[i++] = n % 10 + '0';
		n /= 10;
	} while (n > 0);
	length = i - 1;
	for (i = 0; i <= length/2; i++) {
		c1 = s[i];
		s[i] = s[length - i];
		s[length - i] = c1;
	}
	gSynchConsole->Write(s, length + 1);
}

void ReadString_SysCall() {
	int virAddr = machine->ReadRegister(ARG1);
	int length = machine->ReadRegister(ARG2);
	int i;
	char *buff = new char[length + 1];

	for (i = 0; i < length; i++) {
		char chr;
		gSynchConsole->Read(&chr, 1);

		if (chr == '\n') {
			chr = 0;
		}
		buff[i] = chr;
		if (buff[i] == 0) break;
	}
	System2User(virAddr, i+1, buff);
	delete buff;
	machine->WriteRegister(RET, int(0));
}

void PrintString_SysCall() {
	int virAddr = machine->ReadRegister(ARG1);

	while (true) {
		char *chr = User2System(virAddr++, 1);
		if (chr == NULL || *chr == 0) {
			break;
		}
		gSynchConsole->Write(chr, 1);
		delete chr;
	}
	machine->WriteRegister(RET, int(0));
}

void ReadChar_SysCall() {
	int bufferSize;
	char buffer[MAX_INT];
	bufferSize = gSynchConsole->Read(buffer, MAX_INT);
	machine->WriteRegister(2, buffer[bufferSize - 1]);
}

void PrintChar_SysCall() {
    char chr = machine->ReadRegister(ARG1);
    gSynchConsole->Write(&chr, 1);
    machine->WriteRegister(RET, int(0));
}

void increasePC() {
    machine->registers[PrevPCReg] = machine->registers[PCReg];
    machine->registers[PCReg] = machine->registers[NextPCReg];
    machine->registers[NextPCReg] += 4;
}

void ExceptionHandler(ExceptionType which) {
    int type = machine->ReadRegister(2);

    switch (which) {
        case SyscallException:
            switch (type) {
                case SC_Halt:
                    DEBUG('a', "Shutdown, initiated by user program.\n");
                    interrupt->Halt();
                    break;
                case SC_Compare:
                    Compare_SysCall();
                    break;
                case SC_ReadChar:
                    ReadChar_SysCall();
                    break;
                case SC_PrintChar:
                    PrintChar_SysCall();
                    break;
				case SC_ReadInt:
                    ReadInt_SysCall();
                    break;
                case SC_PrintInt:
                    PrintInt_SysCall();
                    break;
                case SC_ReadString:
                    ReadString_SysCall();
                    break;
                case SC_PrintString:
                    PrintString_SysCall();
                    break;
            }
            // increment value in PC register
            increasePC();
    }
}

char* User2System(int virtAddr, int limit) {
	int i;
	int ch;
	char* kernelBuf = NULL;
	kernelBuf = new char[limit+1];
	if (kernelBuf == NULL)
	{
		return kernelBuf;
	}
	memset(kernelBuf,0, limit+1);
	for (i = 0; i < limit; ++i)
	{
		machine->ReadMem(virtAddr+i, 1, &ch);
		kernelBuf[i] = ch;
		if (ch == 0) break;
	}
	return kernelBuf;
}

int System2User(int virtAddr, int len, char* buffer) {
	if (len < 0) return -1;
	if (len == 0) return len;
	int i = 0;
	int ch = 0;
	do {
		ch = (int) buffer[i];
		machine->WriteMem(virtAddr+i, 1, ch);
		i++;
	} while (i<len && ch != 0);
	return i;
}