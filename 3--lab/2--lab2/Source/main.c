#include <stdio.h>
#include "add.h"
#include "subtract.h"
#include "multiply.h"
#include "divide.h"

void input(int *a, int *b);
void output(int *a, int *b);


int main() {
	int a, b;
	input(&a, &b);
	output(&a, &b);
 	
 	return 0;
}

void input(int *a, int *b) {
	printf("---Input---\n");
	printf("a: ");
	scanf("%d", a);
	printf("b: ");
	scanf("%d", b);
}

void output(int *a, int *b) {
	printf("\n---Output---\n");
	printf("a + b = %d\n", add(*a, *b));
 	printf("a - b = %d\n", subtract(*a, *b));
 	printf("a * b = %d\n", multiply(*a, *b));
 	printf("a / b = %d\n", divide(*a, *b));
}