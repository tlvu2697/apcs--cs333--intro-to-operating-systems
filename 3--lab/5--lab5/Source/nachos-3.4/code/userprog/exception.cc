// exception.cc 
//  Entry point into the Nachos kernel from user programs.
//  There are two kinds of things that can cause control to
//  transfer back to here from user code:
//
//  syscall -- The user code explicitly requests to call a procedure
//  in the Nachos kernel.  Right now, the only function we support is
//  "Halt".
//
//  exceptions -- The user code does something that the CPU can't handle.
//  For instance, accessing memory that doesn't exist, arithmetic errors,
//  etc.  
//
//  Interrupts (which can also cause control to transfer from user
//  code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#define RET 2
#define ARG1 4
#define ARG2 5
#define ARG3 6
#define ARG4 7
#define INT_LENGTH 10
#define MAX_INT 65535

//----------------------------------------------------------------------
// ExceptionHandler
//  Entry point into the Nachos kernel.  Called when a user program
//  is executing, and either does a syscall, or generates an addressing
//  or arithmetic exception.
//
//  For system calls, the following is the calling convention:
//
//  system call code -- r2
//      arg1 -- r4
//      arg2 -- r5
//      arg3 -- r6
//      arg4 -- r7
//
//  The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//  "which" is the kind of exception.  The list of possible exceptions 
//  are in machine.h.
//----------------------------------------------------------------------

char* User2System(int virtAddr, int limit) {
    int i;
    int ch;
    char* kernelBuf = NULL;
    kernelBuf = new char[limit+1];
    if (kernelBuf == NULL)
    {
        return kernelBuf;
    }
    memset(kernelBuf,0, limit+1);
    for (i = 0; i < limit; ++i)
    {
        machine->ReadMem(virtAddr+i, 1, &ch);
        kernelBuf[i] = ch;
        if (ch == 0) break;
    }
    return kernelBuf;
}

void CreateFile_SysCall() {
    int bufAddr = machine->ReadRegister(ARG1);
    char *filename = new char[MAX_INT];
    FILE *fp = NULL;

    filename = User2System(bufAddr, MAX_INT);
    fp = fopen(filename ,"a");
    delete[] filename;

    if (fp != NULL) {
        machine->WriteRegister(RET, 1);
    } else {
        machine->WriteRegister(RET, 0);
    }
}

void increasePC() {
    machine->registers[PrevPCReg] = machine->registers[PCReg];
    machine->registers[PCReg] = machine->registers[NextPCReg];
    machine->registers[NextPCReg] += 4;
}

void ExceptionHandler(ExceptionType which) {
    int type = machine->ReadRegister(2);

    switch (which) {
        case SyscallException:
            switch (type) {
                case SC_Halt:
                    DEBUG('a', "Shutdown, initiated by user program.\n");
                    interrupt->Halt();
                    break;
                case SC_CreateFile:
                    CreateFile_SysCall();
                    break;
            }
            // increment value in PC register
            increasePC();
    }
}