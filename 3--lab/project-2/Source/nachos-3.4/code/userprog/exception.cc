// exception.cc
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.


#include "copyright.h"
#include "system.h"
#include "syscall.h"

#define RET 2
#define ARG1 4
#define ARG2 5
#define ARG3 6
#define ARG4 7
#define INT_LENGTH 10
#define MAX_INT 65535

/*----------------------------------------------------------------------

// ExceptionHandler

Entry point into the Nachos kernel.  Called when a user program

/ executing, and either does a syscall, or generates an addressing
or arithmetic exception.
For system calls, the following is the calling convention:

system call code -- r2
arg1 -- r4
arg2 -- r5
arg3 -- r6
arg4 -- r7

The result of the system call, if any, must be put back into r2.
And don't forget to increment the pc before returning. (Or else you'll
loop making the same system call forever
"which" is the kind of exception.  The list of possible exceptions
are in machine.h.
//----------------------------------------------------------------------*/

void increasePC();
void Compare_SysCall();
void ReadChar_SysCall();
void PrintChar_SysCall();
void ReadInt_SysCall();
void PrintInt_SysCall();
void ReadString_SysCall();
void PrintString_SysCall();

char * User2System(int virtAddr, int limit);
int System2User(int virtAddr, int len, char * buffer);

void Compare_SysCall() {
    int op1, op2, result;
    op1 = machine->ReadRegister(4);
    op2 = machine->ReadRegister(5);
    result = op1 - op2;
    if (result > 0) {
        result = 1;
    } else if (result < 0) {
        result = -1;
    }
    machine->WriteRegister(2, result);
}

void ReadChar_SysCall() {
    int bufferSize;
    char buffer[MAX_INT];
    bufferSize = gSynchConsole->Read(buffer, MAX_INT);
    machine->WriteRegister(2, buffer[bufferSize - 1]);
}

void PrintChar_SysCall() {
    char chr = machine->ReadRegister(ARG1);
    gSynchConsole->Write( & chr, 1);
    machine->WriteRegister(RET, int(0));
}

void ReadInt_SysCall() {
    int num, sign = 1;
    char c;
    do {
        gSynchConsole->Read( & c, 1);
    } while (c == '\n' || c == 0);
    if (c == '+' || c == '-') {
        if (c == '-')
            sign = -1;
        gSynchConsole->Read( & c, 1);
    }
    num = 0;
    while (c >= '0' && c <= '9') {
        num = num * 10 + (c - '0');
        gSynchConsole->Read( & c, 1);
    }
    num = sign * num;
    machine->WriteRegister(2, num);
}

void PrintInt_SysCall() {
    char s[INT_LENGTH + 1], nega, c1;
    int i, n, length;
    i = 0;
    nega = '-';
    n = machine->ReadRegister(ARG1);
    if (n < 0) {
        gSynchConsole->Write( & nega, 1);
        n = -n;
    }
    do {
        s[i++] = n % 10 + '0';
        n /= 10;
    } while (n > 0);
    length = i - 1;
    for (i = 0; i <= length / 2; i++) {
        c1 = s[i];
        s[i] = s[length - i];
        s[length - i] = c1;
    }
    gSynchConsole->Write(s, length + 1);
}

void ReadString_SysCall() {
    int virAddr = machine->ReadRegister(ARG1);
    int length = machine->ReadRegister(ARG2);
    int i;
    char * buff = new char[length + 1];

    for (i = 0; i < length; i++) {
        char chr;
        gSynchConsole->Read( & chr, 1);

        if (chr == '\n') {
            chr = 0;
        }
        buff[i] = chr;
        if (buff[i] == 0) break;
    }
    System2User(virAddr, i + 1, buff);
    delete buff;
    machine->WriteRegister(RET, int(0));
}

void PrintString_SysCall() {
    int virAddr = machine->ReadRegister(ARG1);

    while (true) {
        char * chr = User2System(virAddr++, 1);
        if (chr == NULL || * chr == 0) {
            break;
        }
        gSynchConsole->Write(chr, 1);
        delete chr;
    }
    machine->WriteRegister(RET, int(0));
}

void CreateFile_Syscall() {
    int addr = machine->ReadRegister(ARG1);
    char * chr = new char[MAX_INT];
    chr = User2System(addr, MAX_INT);
    int a = (fileSystem->Create(chr, 0)) ? 1 : 0;
    machine->WriteRegister(RET, int(a));
    delete[] chr;
}

bool CheckIfFileIndexMiss(int index){
	if (index < 0 || index > 10 || (fileSystem->openf[index] == NULL)) {
		machine->WriteRegister(RET,-1);
		return true;
	}
	return false;
}

void OpenFileId_Syscall() {
    int bufAddr = machine->ReadRegister(ARG1);
    int type = machine->ReadRegister(ARG2);
    int choose;
    char * buf = new char[MAX_INT];
    if (fileSystem->index >= 10) {
        machine->WriteRegister(2, -1);
        delete[] buf;
        return;
    }
    buf = User2System(bufAddr, MAX_INT);
    if (strcmp(buf, "stdin") == 0) {
        printf("stdin mode\n");
        machine->WriteRegister(2, 0);
        return;
    }
    if (strcmp(buf, "stdout") == 0) {
        printf("stdout mode\n");
        machine->WriteRegister(2, 1);
        return;
    }
    for (choose = 2; choose < 10; choose++) {
        if (fileSystem->openf[choose] == NULL) {
            break;
        }
    }
    if ((fileSystem->openf[choose] = fileSystem->Open(buf, type)) != NULL) {
        DEBUG('f', (char*)"open file successfully");
        machine->WriteRegister(2, choose); 
    } else {
        DEBUG('f', (char*)"can not open file");
        machine->WriteRegister(2, -1);
    };
    delete[] buf;
}

void CloseFileId_Syscall() {
	//OpenF is an array of openfiles. Given an index.
	int i = machine->ReadRegister(ARG1);
	if (CheckIfFileIndexMiss(i)) 
		return;
	//Check if exist. If it is, delete it.
	delete fileSystem->openf[i];
	//The magic is done in FileHeader Deallocate
	fileSystem->openf[i] = NULL;
    fileSystem->index--;
	return;
}

void ReadFile_Syscall() {
	int addr, size, pos, index;
	addr = machine->ReadRegister(ARG1);
	size = machine->ReadRegister(ARG2);
	index = machine->ReadRegister(ARG3);
	char* buf = new char[size];
	//Check if with the given index, the openfile exist
	if (CheckIfFileIndexMiss(index)) {
		delete[] buf;
		return;
	}
	OpenFile* file = fileSystem->openf[index];
	pos = file->GetCurrentPos();
	buf = User2System(addr, size);
	if (index == 0) //stdin mode
	{
		int sz = gSynchConsole->Read(buf, size);
		System2User(addr, sz, buf);
		machine->WriteRegister(RET,sz);
		return;
	}
	if ((file->Read(buf, size)  ) > 0) { //Other file type
		pos = file->GetCurrentPos() - pos + 1;
		//Pos now store the position different
		System2User(addr, pos, buf);
		machine->WriteRegister(RET, pos);
	}
	else
		machine->WriteRegister(RET, -1);
	delete[] buf;
}

void WriteFile_Syscall() {
	int addr, size, pos, index;
	addr = machine->ReadRegister(ARG1);
	size = machine->ReadRegister(ARG2);
	index = machine->ReadRegister(ARG3);
	char* buf = new char[size];
	//Check if with the given index, the openfile exist
	if (CheckIfFileIndexMiss(index)) {
		delete[] buf;
		return;
	}
	
	OpenFile* file = fileSystem->openf[index];
	
	pos = file->GetCurrentPos();
	buf = User2System(addr, size);
	if (file->type == 1){ //Read Only
		machine->WriteRegister(2, -1);
		delete[] buf;
		return;
	}
	//Standard File
	if ((fileSystem->openf[index]->Write(buf, size)) > 0) 
	{
	// Copy data from kernel to user space
		machine->WriteRegister(RET, file->GetCurrentPos() - pos + 1);
	}
	if (index == 1) {
		pos = 0;
		while (buf[pos] != 0 && buf[pos] != '\n') //For each character in the buffer, write the char on the screen.
		{
			gSynchConsole->Write(buf+pos, 1);
			pos++;
		}
		buf[pos] = '\0';
		gSynchConsole->Write(buf+pos, 1);
		machine->WriteRegister(RET, pos-1);
	}
	delete[] buf;
}

void SeekFile_Syscall() {
	int pos, index;
	pos = machine->ReadRegister(ARG1);
	index = machine->ReadRegister(ARG2);
	OpenFile* file = fileSystem->openf[index];
	if (CheckIfFileIndexMiss(index))
		return;
	if (pos > file->Length() || pos < -1) {
		machine->WriteRegister(RET, -1);
		return;
	}
	if (pos == -1)
		pos = file->Length();
	file->Seek(pos);
	machine->WriteRegister(RET, pos);
}

void increasePC() {
    machine->registers[PrevPCReg] = machine->registers[PCReg];
    machine->registers[PCReg] = machine->registers[NextPCReg];
    machine->registers[NextPCReg] += 4;
}

char * User2System(int virtAddr, int limit) {
    int i;
    int ch;
    char * kernelBuf = NULL;
    kernelBuf = new char[limit + 1];
    if (kernelBuf == NULL) {
        return kernelBuf;
    }
    memset(kernelBuf, 0, limit + 1);
    for (i = 0; i < limit; ++i) {
        machine->ReadMem(virtAddr + i, 1, & ch);
        kernelBuf[i] = ch;
        if (ch == 0) break;
    }
    return kernelBuf;
}

int System2User(int virtAddr, int len, char * buffer) {
    if (len < 0) return -1;
    if (len == 0) return len;
    int i = 0;
    int ch = 0;
    do {
        ch = (int) buffer[i];
        machine->WriteMem(virtAddr + i, 1, ch);
        i++;
    } while (i < len && ch != 0);
    return i;
}

void ExceptionHandler(ExceptionType which) {
    int type = machine->ReadRegister(2);

    switch (which) {
    case SyscallException:
        switch (type) {
        case SC_Halt:
            DEBUG('a', (char*)"Shutdown, initiated by user program.\n");
            interrupt->Halt();
            break;
        case SC_Compare:
            Compare_SysCall();
            break;
        case SC_ReadChar:
            ReadChar_SysCall();
            break;
        case SC_PrintChar:
            PrintChar_SysCall();
            break;
        case SC_ReadInt:
            ReadInt_SysCall();
            break;
        case SC_PrintInt:
            PrintInt_SysCall();
            break;
        case SC_ReadString:
            ReadString_SysCall();
            break;
        case SC_PrintString:
            PrintString_SysCall();
            break;
        case SC_CreateFile:
            CreateFile_Syscall();
            break;
        case SC_OpenFileId:
            OpenFileId_Syscall();
            break;
        case SC_CloseFile:
			CloseFileId_Syscall();
            break;
        case SC_ReadFile:
			ReadFile_Syscall();
            break;
        case SC_WriteFile:
			WriteFile_Syscall();
            break;
        case SC_SeekFile:
			SeekFile_Syscall();
            break;
        default:
            break;
        }
    }
	increasePC(); // increment value in PC register
}