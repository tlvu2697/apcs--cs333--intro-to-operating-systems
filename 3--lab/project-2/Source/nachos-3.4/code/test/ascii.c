/* ascii.c 
 *    Test program to list ascii table.
 *
 */
#include "syscall.h"
int main() {
	int i, j;
	PrintString("---ASCII Table---\n");
	PrintString("Dec - Char\n");
    for (i = 0; i < 16; i++) {
		for (j = 0; j < 16; j++) {
			PrintInt(i * 16 + j);
			PrintString(" - ");
			PrintChar((char) (i * 16 + j));
			PrintString("\t");
		}
    	PrintString("\n");
    }
}
