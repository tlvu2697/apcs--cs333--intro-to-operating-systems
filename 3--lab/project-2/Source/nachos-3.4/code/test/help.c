/* help.c 
 *     print the simply introduction about your team
 *		and briefly description of sort and ascii program
 *		(refer to PrintString(char[]) system call)
 *
 */

#include "syscall.h"

int
main()
{
    char* na1, na2, id1, id2, sort1, ascii1;
	PrintString("Name :      Tran Thoai Thong\n");
	PrintString("Student ID: 1551039\n");
	PrintString("Name :      Tran Le Vu\n");
	PrintString("Student ID: 1551048\n");
	PrintString("\n	Function: Sort\nAn array with n elements. ");
	PrintString("Sort the above array.\n");
	PrintString("\n	Function: Ascii\nPrint ascii character table.\n");
	return 0;
}
