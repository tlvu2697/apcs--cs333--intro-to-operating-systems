#include "syscall.h"

int main()
{
	char c, name[100];
	int fId, size, i;

	PrintString("Input Name:");
	ReadString(name, 100);
	fId = OpenFileFunc(name, 1);
	if (fId == -1)
	{
		PrintString("Cannot Open File");
		return 0;
	}
	size = SeekFile(-1, fId);
	SeekFile(0, fId);
	for (i = 0; i < size; ++i)
	{
		ReadFile(&c, 1, fId);
		PrintChar(c);
	}
	CloseFile(fId);
	return 0;
}
