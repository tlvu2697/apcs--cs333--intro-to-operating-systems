/* compare.c 
 *    Test program to compare 2 integers.
 *
 */
#include "syscall.h"
int
main()
{
	int i;
    for (i = 0; i < 128; i++) {
    	PrintInt(i);
    	PrintString(" - ");
    	PrintChar((char) i);
    	PrintString("\n");
    }
}
