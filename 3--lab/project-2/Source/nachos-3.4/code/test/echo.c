#include "syscall.h"
#define size 255

int main()
{
	char mess[size];
	int i;
	OpenFileId inCon = OpenFileFunc("stdin", 0);
	OpenFileId outCon = OpenFileFunc("stdout", 1);
	if (inCon == -1 || outCon == -1)
	{
		PrintString("Fail to open\n");
		return 0;
	}
	ReadFile(mess, 255, inCon);
	WriteFile(mess, 255, outCon);
	CloseFile(inCon);
	CloseFile(outCon);
	return 0;
}
