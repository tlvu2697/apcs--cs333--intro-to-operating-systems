#include "syscall.h"
#define size 100

int main()
{
	int srcSz, i;
	char tmpChar, filename1[size + 1], filename2[size + 1];
	OpenFileId id1, id2;

	PrintString("Input source file name: ");
	ReadString(filename1, size + 1);
	id1 = OpenFileFunc(filename1, 1);

	PrintString("Input destination file name: ");
	ReadString(filename2, size + 1);
	id2 = OpenFileFunc(filename2, 0);

	srcSz = SeekFile(-1, id1);
	for (i = srcSz - 1; i >= 0; i--)
	{
		SeekFile(i, id1);
		ReadFile(&tmpChar, 1, id1);
		if (tmpChar != '\n')
			WriteFile(&tmpChar, 1, id2);
	}

	PrintString("Reverse Successfully");
	CloseFile(id1);
	CloseFile(id2);
	return 0;
}