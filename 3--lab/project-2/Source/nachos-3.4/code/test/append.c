#include "syscall.h"
#define size 100

int main()
{
	OpenFileId src1, src2, dstId;
	int i, fSize;
	char address[size], c;
	//Input the file names. The input files and the output file
	//Input file 1
	PrintString("Input source file 1:");
	ReadString(address, size);
	src1 = OpenFileFunc(address, 1);
	//Input file 2
	PrintString("Input source file 2:");
	ReadString(address, size);
	src2 = OpenFileFunc(address, 1);
	//Output file
	PrintString("Input destination file:");
	ReadString(address, size);
	//The Ids store the openfile ID to be referenced later.
	CreateFile(address);
	dstId = OpenFileFunc(address, 0);
	//If either file cannot created.
	if (src1 == -1 || src2 == -1 || dstId == -1)
	{
		PrintString("Error\n");
		return 0;
	}
	//Finish Open Files
	//Get to the end of file

	SeekFile(0, dstId);

	fSize = SeekFile(-1, src1);
	SeekFile(0, src1);
	//From begining to end of file1
	for (i = 0, c; i < fSize; ++i)
	{
		ReadFile(&c, 1, src1);
		WriteFile(&c, 1, dstId);
	}
	fSize = SeekFile(-1, src2);
	SeekFile(0, src2);
	//From begining to end of file2
	for (i = 0, c; i < fSize; ++i)
	{
		ReadFile(&c, 1, src2);
		WriteFile(&c, 1, dstId);
	}
	CloseFile(src1);
	CloseFile(src2);
	CloseFile(dstId);
	return 0;
}