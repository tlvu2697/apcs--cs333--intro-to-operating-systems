#include "syscall.h"

int main()
{
	int created = CreateFile("abc.txt");
	int open;
	if (created)
	{
		PrintString("Can not create file\n");
	}
	else
	{
		PrintString("Successfully create file\n");
	}
}
