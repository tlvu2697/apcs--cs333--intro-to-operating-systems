#include "syscall.h"
#define size 100

int main()
{
	int srcId, dstId, filesz, i;
	char source[size], dest[size], c;
	//Input the file names. The input file and the output file
	PrintString("Input source file:");
	ReadString(source, size);
	PrintString("Input destination file:");
	ReadString(dest, size);
	PrintString(source);
	//The Ids store the openfile ID to be referenced later.
	srcId = OpenFileFunc(source, 1);
	CreateFile(dest);
	dstId = OpenFileFunc(dest, 0);
	//If either file cannot created.
	if (srcId == -1 || dstId == -1)
	{
		PrintString("Error\n");
		return 0;
	}
	//Finish Open Files
	//Get to the end of file

	filesz = SeekFile(-1, srcId);
	SeekFile(0, srcId);
	SeekFile(0, dstId);
	//From begining to end of file
	for (i = 0, c; i < filesz; ++i)
	{
		ReadFile(&c, 1, srcId);
		WriteFile(&c, 1, dstId);
	}
	CloseFile(srcId);
	CloseFile(dstId);
	return 0;
}
