/* compare.c 
 *    Test program to compare 2 integers.
 *
 */

#include "syscall.h"

int
main()
{
    int result;
    result = Compare(40, 60);
    Exit(result);
}
